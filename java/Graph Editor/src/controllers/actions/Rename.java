package controllers.actions;

import javax.swing.undo.AbstractUndoableEdit;
import models.GraphVertex;
import controllers.*;

public class Rename extends AbstractUndoableEdit {
	private static final long serialVersionUID = -7929100929760152271L;
	private GraphVertex o;
	private String savedName;
	private int originalWidth;
	private SelectionController selectionController;
	
	public Rename(String name, SelectionController selectionController)
	{
		this.selectionController = selectionController;
		this.savedName = name;
		this.o = selectionController.getSelectedVertex();
		originalWidth = o.getWidth();
		this.redoAction();
	}
		
	private void redoAction(){
		int width = 20 + this.selectionController.getPanel().getDrawTextWidth(savedName);
		if(width > o.getWidth())
			o.setWidth(width);
		
		String objectName = o.getName();
		o.setName(savedName);
		savedName = objectName;
	}
	public void redo()
	{
		super.redo();
		redoAction();
	}
	
	public void undo()
	{
		super.undo();
		String objectName = o.getName();
		o.setName(savedName);
		o.setWidth(originalWidth);
		savedName = objectName;
	}
	public boolean isSignificant(){
		return true;
	}
	
	public String getPresentationName(){
		return "Vertex Hernoemen.";
	}
}
