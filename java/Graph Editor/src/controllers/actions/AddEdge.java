package controllers.actions;
import javax.swing.undo.AbstractUndoableEdit;

import models.GraphEdge;
import models.GraphVertex;
import controllers.*;

public class AddEdge extends AbstractUndoableEdit {
	private static final long serialVersionUID = -7929100929760152271L;
	private GraphEdge o;
	private SelectionController selectionController;
	
	public AddEdge(GraphVertex a, GraphVertex b,SelectionController selectionController)
	{
		o = new GraphEdge(a,b);
		this.selectionController = selectionController;
		redoAction();
	}
	
	private void redoAction()
	{
		selectionController.getModel().addEdge(o);
	}
	
	public void redo()
	{
		super.redo();
		redoAction();
	}
	
	public void undo()
	{
		super.undo();
		selectionController.getModel().removeEdge(o);
	}
	
	public boolean isSignificant(){
		return true;
	}
	
	public String getPresentationName(){
		return "Edge toevoegen.";
	}
}
