package controllers.actions;
import javax.swing.undo.AbstractUndoableEdit;
import models.GraphVertex;

public class Move extends AbstractUndoableEdit {
	private static final long serialVersionUID = -7929100929760152271L;
	private GraphVertex o;
	private int originalX, originalY, newX, newY;
	
	public Move(int originalX, int originalY,
				int newX, int newY, GraphVertex o)
	{
		this.o = o;
		this.originalX = originalX;
		this.originalY = originalY;
		this.newX = newX;
		this.newY = newY;
		redoAction();
	}
	
	private void redoAction(){
		o.setX(newX);
		o.setY(newY);
	}
	public void redo()
	{
		super.redo();
		redoAction();
	}
	
	public void undo()
	{
		super.undo();
		o.setX(originalX);
		o.setY(originalY);
	}
	
	public boolean isSignificant(){
		return true;
	}
	
	public String getPresentationName(){
		return "Vertex verplaatsen.";
	}
}
