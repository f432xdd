package controllers.actions;

import javax.swing.undo.AbstractUndoableEdit;

import models.GraphEdge;
import models.GraphVertex;
import controllers.*;

public class DeleteNode extends AbstractUndoableEdit {
	private static final long serialVersionUID = -7929100929760152271L;
	private GraphVertex o;
	private SelectionController selectionController;
	private GraphEdge[] affectedEdges;
	
	public DeleteNode(SelectionController selectionController)
	{
		this.selectionController = selectionController;
		this.o = selectionController.getSelectedVertex();
		redoAction();
	}
	
	private void removeAffectedEdges()
	{
		selectionController.getModel().removeEdge(affectedEdges);
	}
	
	private void addAffectedEdges()
	{
		selectionController.getModel().addEdge(affectedEdges);
	}
	
	private void getAffectedEdges(){
		GraphEdge[] arr = selectionController.getModel().getEdges();
		int count = 0;
		for(int i=0; i < arr.length; i++)
			if(arr[i].EdgeOf(o))
				count++;
		affectedEdges = new GraphEdge[count];
		count = 0;
		for(int i=0; i < arr.length; i++)
			if(arr[i].EdgeOf(o))
				affectedEdges[count++] = arr[i];
	}
	
	private void redoAction()
	{
		getAffectedEdges();
		removeAffectedEdges();
		selectionController.removeSelected();
		selectionController.getModel().removeGraph(o);
	}
	
	public void redo()
	{
		super.redo();
		redoAction();
	}
	
	public void undo()
	{
		super.undo();
		addAffectedEdges();
		selectionController.getModel().addGraph(o);
		selectionController.setSelected(o);
	}
	public boolean isSignificant(){
		return true;
	}
	
	public String getPresentationName(){
		return "Vertex verwijderen.";
	}
}
