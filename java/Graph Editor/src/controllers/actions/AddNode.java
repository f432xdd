package controllers.actions;

import javax.swing.undo.AbstractUndoableEdit;

import controllers.SelectionController;

import models.GraphVertex;

public class AddNode extends AbstractUndoableEdit {
	private static final long serialVersionUID = -7929100929760152271L;
	private SelectionController selectionController;
	private GraphVertex o;
	
	

	public AddNode(SelectionController selectionController)
	{
		this.selectionController = selectionController;
		o = new GraphVertex();
		redoAction();
	}
	private void redoAction(){
		selectionController.getModel().addGraph(o);
		selectionController.setSelected(o);
	}
	
	public void redo()
	{
		super.redo();
		redoAction();
	}
	
	public void undo()
	{
		super.undo();
		selectionController.getModel().removeGraph(o);
		selectionController.removeSelected();		
	}
	
	public boolean isSignificant(){
		return true;
	}
	
	public String getPresentationName(){
		return "Vertex toevoegen.";
	}
}
