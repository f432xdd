package controllers;

import interfaces.*;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import models.GraphModel;
import models.GraphVertex;

import controllers.actions.AddEdge;
import controllers.actions.Move;
import controllers.actions.Rename;

public class SelectionController implements MouseListener,MouseMotionListener,KeyListener{
	private GraphPanel object = null;
	private GraphFrame frame = null;
	private GraphVertex selectedObject = null;
	private GraphModel model = null;
	
	private String typedStr;
	private String originalName;
	private boolean isChangingName;
	private int OriginalNameLength;
	
	private boolean isAddingEdge;
	private GraphVertex edgeObject1;
	
	
	private int clickX = 0;
	private int clickY = 0;
	private int mouseClickX =0;
	private int mouseClickY =0;
	
	private int mouseX = 0;
	private int mouseY = 0;
	
	public int getMouseX(){
		return mouseX;
	}
	public int getMouseY(){
		return mouseY;
	}
	
	
	public SelectionController(GraphFrame frame, GraphModel model){
		isChangingName = false;
		isAddingEdge = false;
		this.frame = frame;
		frame.setSelectionController(this);		
		frame.addKeyListener(this);
		
		setObject(frame.getPanel());
		setModel(model);
	}
	
	public void doAddEdge()
	{
		if(isChangingName){
			this.doChangeName();
		}
		isAddingEdge = true;
		edgeObject1 = this.getSelectedVertex();
	}
	
	public boolean isAddingEdge(){
		return isAddingEdge;
	}
	
	private void finishAddingEdge(){
		frame.addAction(new AddEdge(edgeObject1,this.getSelectedVertex(),this));
		isAddingEdge = false;
	}
	
	public GraphModel getModel() {
		return model;
	}
	public void setSelected(GraphVertex o)
	{
		frame.selectedObject(o != null);
		if(isChangingName)
			this.doChangeName();
		
		if(selectedObject != null)
			selectedObject.setSelected(false);
		selectedObject = o;
		
		if(selectedObject != null){
			o.setSelected(true);
			if(isAddingEdge){
				finishAddingEdge();
			}
		}else{
			isAddingEdge = false; // Didn't click object, cancel action			
		}
	}
	public GraphVertex getSelectedVertex()
	{
		return selectedObject;
	}
	
	public void removeSelected()
	{
		this.setSelected(null);
	}
	

	public void setObject(GraphPanel panel){
		if(object != null){
			object.removeMouseListener(this);
			object.removeMouseMotionListener(this);
			object.removeKeyListener(this);
		}
		object = panel;
		object.addMouseListener(this);
		object.addMouseMotionListener(this);
		object.addKeyListener(this);
	}

	public void setModel(GraphModel model){
		removeSelected();
		this.model = model;
	}
	
	
	// Called when the typing stops, Either Return has been pressed or a
	// other object has been selected.
	private void doChangeName(){
		selectedObject.setName(originalName);
		this.frame.addAction(new Rename(typedStr,this));
		this.isChangingName = false;
	}
	
	public GraphPanel getPanel()
	{
		return this.object;
	}
	
	private void doCancelNameChange(){
		selectedObject.setName(this.originalName);
		selectedObject.setWidth(this.OriginalNameLength);
		isChangingName = false;
	}
	
	
	private void handleDoubleClick()
	{
		if(selectedObject != null){
			this.typedStr = "";
			isChangingName = true;
			originalName = selectedObject.getName();
			OriginalNameLength = selectedObject.getWidth();	
		}
	}
	
	public void mouseClicked(MouseEvent arg0) {	
		if(arg0.getButton() == MouseEvent.BUTTON1){
			if(arg0.getClickCount() == 2){
				handleDoubleClick();
			}else{				
				this.setSelected(model.getGraphByCoordinate(arg0.getX(), arg0.getY()));
			}
		}
	}

	public void mouseEntered(MouseEvent arg0) {
	}

	public void mouseExited(MouseEvent arg0) {
	}

	public void mousePressed(MouseEvent arg0) {
		this.setSelected(model.getGraphByCoordinate(arg0.getX(), arg0.getY()));
		
		clickX = arg0.getX();
		clickY = arg0.getY();
		
		mouseClickX = arg0.getX();
		mouseClickY = arg0.getY();
	}

	public void mouseReleased(MouseEvent arg0) {
		if(selectedObject != null){
			if(arg0.getX() != mouseClickX || arg0.getY() != mouseClickY){
				int newX = this.getSelectedVertex().getX();
				int newY = this.getSelectedVertex().getY();
				int orgX = newX - ( arg0.getX() - mouseClickX);
				int orgY = newY - ( arg0.getY() - mouseClickY);
				frame.addAction(new Move(orgX,orgY,newX,newY,this.getSelectedVertex()));
			}
		}
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		if(selectedObject != null){
			selectedObject.setX(selectedObject.getX() + (arg0.getX() - clickX));
			selectedObject.setY(selectedObject.getY() + (arg0.getY() - clickY));
			clickX = arg0.getX();
			clickY = arg0.getY();
		}
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		this.mouseX = arg0.getX();
		this.mouseY = arg0.getY();
		if(this.isAddingEdge){
			this.object.repaint();
		}
	}
	
	private void setGraphText(GraphVertex o, String text){
		int width = 20 + this.getPanel().getDrawTextWidth(text);
		o.setWidth(width);
		o.setName(text);
	}
	
	private void removeLastCharacterFromInput(){
		this.typedStr = this.typedStr.substring(0,this.typedStr.length()-1);
		setGraphText(selectedObject,typedStr);
	}
	
	private void handleKey(int key)
	{
		switch(key){
			case KeyEvent.VK_ENTER:
				doChangeName();
				break;
			case KeyEvent.VK_BACK_SPACE:
				removeLastCharacterFromInput();
				break;
			case KeyEvent.VK_ESCAPE:
				doCancelNameChange();
				break;
			default:
				break;
		}
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		int key = arg0.getKeyCode();
		if(this.isChangingName && selectedObject != null){
			handleKey(key);
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		
	}

	private boolean acceptKeyForName(char a){
		String l = new String();
		l += a;
		String acceptKeys = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789~!@#$%^&*()_+=-`[]{};'\":<>?,./\\| «①∞⁵‰‽ﬂ○";
		return acceptKeys.contains(l);
	}
	
	@Override
	public void keyTyped(KeyEvent arg0) {
		// Todo Fix VK_ENTER + VK_ESCAPE
		if(this.isChangingName && selectedObject != null && !arg0.isActionKey()){			
				if(arg0.getKeyChar() != KeyEvent.CHAR_UNDEFINED && acceptKeyForName(arg0.getKeyChar())){
					this.typedStr += arg0.getKeyChar();
					setGraphText(selectedObject,typedStr);
				}
		}
	}
}
