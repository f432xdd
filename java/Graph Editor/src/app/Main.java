package app;

import controllers.*;
import models.*;
import interfaces.*;

public class Main {
	private GraphModel model;
	private GraphFrame frame;

	private boolean twoWindows = false;
	public Main(String[] args){
		if(args.length > 0){
			try{
				model = GraphModel.fromFile(args[0]);
			}catch(Exception e){
				javax.swing.JOptionPane.showMessageDialog(null, "Error reading file!");
				model = new GraphModel();
			}
		}else{
			model = new GraphModel();
		}
		
		frame = new GraphFrame(model);
		new SelectionController(frame, model);
		
		if(twoWindows){
			//Test code voor 2 vensters!
			GraphFrame frame2 = new GraphFrame(model);
			new SelectionController(frame2, model);
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Main(args);
	}

}
