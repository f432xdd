package models;

import java.awt.Rectangle;
import java.util.Observable;
import java.util.Scanner;

public class GraphVertex extends Observable{
	private String name;
	private Rectangle shape;
	private boolean selected;
	
	public boolean isSelected(){
		return selected;
	}

	
	public GraphVertex(String name){
		this.name = name;
		shape = new Rectangle();
		shape.height = 50;
		shape.width = 100;
	}
	
	public GraphVertex(Scanner s){
		shape = new Rectangle();
		this.shape.x = s.nextInt();
		this.shape.y = s.nextInt();
		this.shape.width = s.nextInt();
		this.shape.height = s.nextInt();
		s.skip(" ");
		this.name = s.nextLine();
	}
	
	public GraphVertex(){
		this("default.");
	}
	
	public int getX(){
		return shape.x;
	}

	public int getY(){
		return shape.y;
	}

	public void setX(int x){
		shape.x = x;
		setChanged();
	}

	public void setY(int y){
		shape.y = y;
		setChanged();
	}

	public synchronized void setChanged() { 
	       super.setChanged(); 
	       notifyObservers();
	}

	public int getWidth(){
		return shape.width;
	}
	
	public void setWidth(int w){
		shape.width = w;
		setChanged();
	}
	
	public void setHeight(int h){
		shape.height = h;
		setChanged();
	}

	public int getHeight(){
		return shape.height;
	}
	
	protected boolean isAt(int x, int y){
		if(x >= shape.x && x <= shape.x + shape.width)
			if(y >= shape.y && y <= shape.y + shape.height)
				return true;
		return false;
	}
	
	public void setSelected(boolean flag){
		selected = flag;	
		setChanged();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		setChanged();
	}
}
