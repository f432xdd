package models;

import java.io.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;


public class GraphModel extends Observable implements Observer{
	private List<GraphEdge>		edges;
	private List<GraphVertex> 	vertexes;
	
	
	public GraphVertex[] getVertexes(){
		GraphVertex[] a = new GraphVertex[vertexes.size()];
		return vertexes.toArray(a);
	}
	public GraphEdge[] getEdges(){
		GraphEdge[] a = new GraphEdge[edges.size()];
		return edges.toArray(a);
	}
	
	public GraphModel(){
		edges = new ArrayList<GraphEdge>();
		vertexes = new ArrayList<GraphVertex>();
	}
	
	public synchronized void setChanged() { 
	       super.setChanged(); 
	       notifyObservers();
	} 

	public void addEdge(GraphEdge ... edge){
		for(int i=0; i < edge.length; i++){
			edges.add(edge[i]);
			edge[i].addObserver(this);
		}
		setChanged();
	}
	
	public void addGraph(GraphVertex ... vertex){
		for(int i=0; i < vertex.length; i++){
			vertexes.add(vertex[i]);
			vertex[i].addObserver(this);
		}
		setChanged();
	}
	
	public void removeEdge(GraphEdge ... edge){
		for(int i=0; i < edge.length; i++){
			edges.remove(edge[i]);
			edge[i].deleteObserver(this);
		}
		setChanged();
	}
	public void removeGraph(GraphVertex ... vertex){
		for(int i=0; i < vertex.length; i++){
			vertexes.remove(vertex[i]);
			vertex[i].deleteObserver(this);
		}
		setChanged();
	}
	
	public GraphVertex getGraphByCoordinate(int x, int y){
		for(int i=0; i < vertexes.size(); i++){
			if(vertexes.get(i).isAt(x, y))
				return vertexes.get(i);
		}
		return null;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if(vertexes.contains(arg0) || edges.contains(arg0)){
			this.setChanged();
		}
	}
	
	
	//File IO Code
	public void writeToFile(File file) throws Exception
	{ 
		OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
		
		out.write(vertexes.size() + " " + edges.size() + "\r\n");
		for(int i=0; i < vertexes.size(); i++)
			out.write(vertexes.get(i).getX() + " " + vertexes.get(i).getY() + " " + vertexes.get(i).getWidth() + " " + vertexes.get(i).getHeight() +" "+ vertexes.get(i).getName() + "\r\n");
		for(int i=0; i < edges.size(); i++)
			out.write(vertexes.indexOf(edges.get(i).getEdge(true)) + " " + vertexes.indexOf(edges.get(i).getEdge(false)) + "\r\n");
		out.close();
	}
	

	
	private static GraphVertex[] readVertexes(int count, Scanner s)
	{
		GraphVertex[] vertexes = new GraphVertex[count];
		for(int i=0; i < count; i++)
			vertexes[i] = new GraphVertex(s);
		return vertexes;
	}
	
	
	public static GraphModel fromFile(File file) throws Exception
	{
		GraphModel model = new GraphModel();
		Scanner scanner = new Scanner(file, "UTF-8");
		
		int numKnopen = scanner.nextInt();
		int numKanten = scanner.nextInt();
		scanner.nextLine();
		
		GraphVertex[] vertexes = readVertexes(numKnopen,scanner);
		
		model.addEdge(readKanten(scanner, numKanten, vertexes));
		model.addGraph(vertexes);
		
		return model;
	}
	private static GraphEdge[] readKanten(Scanner scanner, int numKanten,
		GraphVertex[] vertexes) {
		GraphEdge[] edges = new GraphEdge[numKanten];
		for (int i = 0; i < numKanten; i++) {
			int obj1 = scanner.nextInt();
			int obj2 = scanner.nextInt();
			edges[i] = new GraphEdge(vertexes[obj1], vertexes[obj2]);
			if(i != (numKanten-1)){
				scanner.nextLine();
			}
		}
		return edges;
	}
	
	
	public static GraphModel fromFile(String fileName) throws Exception
	{
		File file = new File(fileName);
		return fromFile(file);
	}

}
