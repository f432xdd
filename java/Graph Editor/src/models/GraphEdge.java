package models;

import java.util.Observable;

public class GraphEdge extends Observable{
	private GraphVertex[] nodes;
	
	public synchronized void setChanged() { 
	       super.setChanged(); 
	       notifyObservers();
	}
	
	public GraphEdge(GraphVertex ... nodes){
		this.nodes = nodes;
	}

	public GraphEdge(){
	}
	
	public boolean EdgeOf(GraphVertex o){
		for(int i=0; i < nodes.length; i++)
			if(nodes[i] == o)
				return true;
		return false;
	}
	
	protected GraphVertex getEdge(boolean src){
		if(src)
			return nodes[0];
		else
			return nodes[1];
	}
	
	public void setNodes(GraphVertex[] nodes){
		this.nodes = nodes;
		setChanged();
	}
	
	public GraphVertex[] getNodes(){
		return nodes;
	}
}
