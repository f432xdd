package interfaces;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.util.Observable;
import java.util.Observer;

import javax.swing.*;

import controllers.SelectionController;
import models.GraphEdge;
import models.GraphModel;
import models.GraphVertex;

public class GraphPanel extends JPanel implements Observer{
	private static final long serialVersionUID = 1388592199136335514L;
	private GraphModel model;
	private SelectionController selectionController;
	
	protected GraphPanel(GraphModel model){
		this.setSize(500, 400);
		this.setBackground(Color.BLUE);
		this.model = model;
		model.addObserver(this);
	}
	
	public void setSelectionController(SelectionController controller)
	{
		this.selectionController = controller;
	}
	
	public void setModel(GraphModel model){
		if(this.model != null){
			this.model.deleteObserver(this);
		}
		this.model = model;
		model.addObserver(this);
		selectionController.setModel(model);
		repaint();
	}
	
	public GraphModel getModel(){
		return model;
	}
	
	public int getDrawTextWidth(String in)
	{
		FontMetrics fm = this.getFontMetrics(this.getFont());
		int width = fm.stringWidth(in);
		return width;
	}
	
	
	private void fillBackGround(Graphics g){
		g.setColor(this.getBackground());
		g.fillRect(0, 0, getWidth(), getHeight());
	}
	
	private void drawEdges(Graphics g){
		GraphEdge[] edges = model.getEdges();
		g.setColor(Color.cyan);
		for(int i=0; i < edges.length; i++){
			GraphEdge edge = edges[i];
			GraphVertex[] vertexes = edge.getNodes();
			g.drawLine(vertexes[0].getX() + (vertexes[0].getWidth()/2), 
					vertexes[0].getY() + (vertexes[0].getHeight()/2),
					vertexes[1].getX() + (vertexes[1].getWidth()/2),
					vertexes[1].getY() + (vertexes[0].getHeight()/2));
		}
	}
	
	private void drawVertexLine(Graphics g)
	{
		// Draw Adding vertex line if needed
		if(this.selectionController != null && this.selectionController.isAddingEdge()){
			
			g.setColor(Color.red);
			GraphVertex origen = this.selectionController.getSelectedVertex();
			g.drawLine( origen.getX() + (origen.getWidth()/2), 
						origen.getY() + (origen.getHeight()/2),
						selectionController.getMouseX(),selectionController.getMouseY());
		}	
	}
	
	private void drawVertixes(Graphics g)
	{
		GraphVertex[] objects = model.getVertexes();
		for(int i=0; i < objects.length; i++){
			GraphVertex o = objects[i];
			g.setColor(Color.white);
			g.fillRect(o.getX(), o.getY(), o.getWidth(), o.getHeight());
			if(o.isSelected()){
				g.setColor(Color.red);
			}else{
				g.setColor(Color.black);
			}
			g.drawRect(o.getX(), o.getY(),o.getWidth(), o.getHeight());
			g.setColor(Color.black);
			g.drawString(o.getName(), o.getX()+10, o.getY()+25);
		}
	}
	
	public void paintComponent(Graphics g)
	{
		fillBackGround(g);
		drawEdges(g);
		drawVertexLine(g);
		drawVertixes(g);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if(arg0 == model){
			repaint();
		}
	}
}
