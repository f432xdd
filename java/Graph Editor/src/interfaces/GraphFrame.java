package interfaces;

import models.*;
import controllers.SelectionController;
import controllers.actions.*;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.*;
import javax.swing.undo.*;


public class GraphFrame extends JFrame {
	private static final long serialVersionUID = -7929100929760152271L;
	private SelectionController selectionController;
	private GraphPanel panel;
	private UndoManager undoManager;
	
	private JMenuBar menubar;
	private JMenu menuOptionFile;
	private JMenu menuOptionEdit;
	
	private JMenuItem menuOptionFileOpen;
	private JMenuItem menuOptionFileSave;
	private JMenuItem menuOptionFileClose;
	
	private JMenuItem menuOptionEditUndo;
	private JMenuItem menuOptionEditRedo;
	private JMenuItem menuOptionEditAddNode;
	private JMenuItem menuOptionEditAddEdge;
	private JMenuItem menuOptionEditRemoveNode;
	
	private ActionListener actionListener;
	
	public GraphFrame(GraphModel model){
		
		this.setTitle("Graph Editor");
		this.setSize(500, 500);
		menubar = new JMenuBar();
		this.setFocusable(true);
		
		menuOptionFile = new JMenu("File");
		menuOptionFile.setMnemonic(KeyEvent.VK_F);
		menuOptionEdit = new JMenu("Edit");
		menuOptionEdit.setMnemonic(KeyEvent.VK_E);
		
		menuOptionFileClose = new JMenuItem("Close program",KeyEvent.VK_C);
		menuOptionFileClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, Event.CTRL_MASK));
		
		menuOptionFileOpen = new JMenuItem("Openen",KeyEvent.VK_O);
		menuOptionFileOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, Event.CTRL_MASK));
		
		menuOptionEditUndo = new JMenuItem("Ongedaan maken",KeyEvent.VK_Z);
		menuOptionEditUndo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, Event.CTRL_MASK));
		
		menuOptionEditAddNode = new JMenuItem("Knoop toevoegen",KeyEvent.VK_N);
		menuOptionEditAddNode.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, Event.CTRL_MASK));
		
		menuOptionEditAddEdge = new JMenuItem("Kant toevoegen",KeyEvent.VK_M);
		menuOptionEditAddEdge.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, Event.CTRL_MASK));
		
		menuOptionEditRemoveNode = new JMenuItem("Delete Node",KeyEvent.VK_D);
		menuOptionEditRemoveNode.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE,Event.SHIFT_MASK));
		
		menuOptionEditRedo = new JMenuItem("Herhalen",KeyEvent.VK_R);
		menuOptionEditRedo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R,Event.CTRL_MASK));
		
		menuOptionFileSave = new JMenuItem("Opslaan",KeyEvent.VK_S);
		menuOptionFileSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,Event.CTRL_MASK));
		
		actionListener = new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				if(arg0.getSource() == menuOptionFileSave)
					saveFile();
				else if(arg0.getSource() == menuOptionFileClose)
					System.exit(0);
				else if(arg0.getSource() == menuOptionFileOpen)
					openFile();
				else if(arg0.getSource() == menuOptionEditUndo) {
					undoManager.undo();
					notifyUndoManagerChanged();
				} else if (arg0.getSource() == menuOptionEditRedo) {
					undoManager.redo();
					notifyUndoManagerChanged();
				} else if(arg0.getSource() == menuOptionEditAddNode) {
					GraphFrame.this.undoManager.addEdit(new AddNode(GraphFrame.this.selectionController));
					notifyUndoManagerChanged();
				} else if(arg0.getSource() == menuOptionEditAddEdge) {
					selectionController.doAddEdge();
					notifyUndoManagerChanged();
				} else if(arg0.getSource() == menuOptionEditRemoveNode) {
					undoManager.addEdit(new DeleteNode(selectionController));
					notifyUndoManagerChanged();
				}
			}
		};
		
		menuOptionFileOpen.addActionListener(actionListener);
		menuOptionFileSave.addActionListener(actionListener);
		menuOptionFileClose.addActionListener(actionListener);
		
		menuOptionEditAddNode.addActionListener(actionListener);
		menuOptionEditAddEdge.addActionListener(actionListener);
		menuOptionEditUndo.addActionListener(actionListener);
		menuOptionEditRedo.addActionListener(actionListener);
		menuOptionEditRemoveNode.addActionListener(actionListener);

		menuOptionFile.add(menuOptionFileOpen);
		menuOptionFile.add(menuOptionFileSave);
		menuOptionFile.addSeparator();
		menuOptionFile.add(menuOptionFileClose);
		
		menuOptionEdit.add(menuOptionEditAddNode);
		menuOptionEdit.add(menuOptionEditAddEdge);
		menuOptionEdit.addSeparator();
		menuOptionEdit.add(menuOptionEditUndo);
		menuOptionEdit.add(menuOptionEditRedo);
		menuOptionEdit.add(menuOptionEditRemoveNode);
		
		menubar.add(menuOptionFile);
		menubar.add(menuOptionEdit);
		setJMenuBar(menubar);
		
		panel = new GraphPanel(model);
		add(panel);
		undoManager = new UndoManager();
		this.notifyUndoManagerChanged();
		
		setVisible(true);
	}
	
	private void openFile()
	{
		final JFileChooser fc = new JFileChooser();
		int returnVal = fc.showOpenDialog(this);
		if(returnVal == JFileChooser.APPROVE_OPTION){
			doOpenFile(fc.getSelectedFile());
		}
	}
	
	private void notifyUndoManagerChanged(){
		menuOptionEditRedo.setEnabled(undoManager.canRedo());
		menuOptionEditUndo.setEnabled(undoManager.canUndo());
		
		menuOptionEditRedo.setText(this.undoManager.getRedoPresentationName());
		menuOptionEditUndo.setText(this.undoManager.getUndoPresentationName());		
	}


	private void doOpenFile(File f) {
		try {
			GraphModel model = GraphModel.fromFile(f);
			panel.setModel(model);
			this.undoManager.discardAllEdits();
			notifyUndoManagerChanged();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this,
				"An error occured trying to read the input file. \r\n" + e.getMessage(), 
				"Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void saveFile()
	{
		final JFileChooser fc = new JFileChooser();
		int returnVal = fc.showSaveDialog(this);
		if(returnVal == JFileChooser.APPROVE_OPTION){
			File file = fc.getSelectedFile();
			try{
				panel.getModel().writeToFile(file);
			}catch(Exception e){
				JOptionPane.showMessageDialog(this, 
				"An error occured trying to save the current state to a file. \r\n" + e.getMessage() ,
				"Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	public void addAction(UndoableEdit action){
		undoManager.addEdit(action);
		notifyUndoManagerChanged();
	}
	
	public void selectedObject(boolean selected) {
		menuOptionEditAddEdge.setEnabled(selected);
		menuOptionEditRemoveNode.setEnabled(selected);
	}
	
	public void setSelectionController(SelectionController s) {
		selectionController = s;
		this.getPanel().setSelectionController(s);
	}
	public GraphPanel getPanel()
	{
		return panel;
	}
	
}
