import List

type Table = [Row]
type Row = [String]
type IntList = [Int]

compilers :: Table
compilers =
  [ ["Compiler", "Universiteit/bedrijf"]
  , ["Helium", "Universiteit Utrecht"]
  , ["NHC", "University of York"]
  , ["GHC", "Microsoft Research"]
  , ["Hugs", "Galois Connections"]
  , ["Hugs.NET", "Galois Connections"]
  , ["O'Haskell", "Oregon Graduate Institute"]
  , ["O'Haskell", "Chalmers University of Technology"]
  , ["HBC", "Chalmers University of Technology"]
  ]
locaties :: Table
locaties =
  [ ["Universiteit/bedrijf", "Land", "Stad"]
  , ["Universiteit Utrecht", "Nederland", "Utrecht"]
  , ["University of York", "Engeland", "York"]
  , ["Microsoft Research", "Engeland", "Cambridge"]
  , ["Galois Connections", "Verenigde Staten", "Beaverton"]
  , ["Oregon Graduate Institute", "Verenigde Staten", "Beaverton"]
  , ["Chalmers University of Technology", "Zweden", "Goteborg"]
  ]

calculateRowLengths :: Row -> IntList
calculateRowLengths (x:xs) = z where z = length(x) : calculateRowLengths(xs)
calculateRowLengths [] = []

getMaxNumbersOfTwoLists :: IntList -> IntList -> IntList
getMaxNumbersOfTwoLists (xa:xsa) (xb:xsb) = z where z = max(xa)(xb) : getMaxNumbersOfTwoLists(xsa)(xsb)
getMaxNumbersOfTwoLists (x:xs) [] = z where z = x : getMaxNumbersOfTwoLists(xs)([])
getMaxNumbersOfTwoLists [] (x:xs) = z where z = x : getMaxNumbersOfTwoLists(xs)([])
getMaxNumbersOfTwoLists [] [] = []

getMaxColumnLengths :: Table -> IntList
getMaxColumnLengths (x:xs) = z where z = getMaxNumbersOfTwoLists(calculateRowLengths(x))(getMaxColumnLengths(xs))
getMaxColumnLengths [] = []

writeTableSeperator :: IntList -> String
writeTableSeperator (x:xs) = z where z = concat(["+", writeStringNTimes(x)("-") , writeTableSeperator(xs)])
writeTableSeperator [] = "+\n"

writeStringNTimes :: Int-> String -> String
writeStringNTimes n s
    | n > 0 = concat([s , writeStringNTimes( n - 1)(s)])
    | otherwise = ""

writeRow:: Row -> IntList -> String
writeRow (x:xs) (xa:xsa) = y where y = concat(["|" , x , writeStringNTimes(xa - length(x))(" ") , writeRow(xs)(xsa)])
writeRow [] [] = "|\n"

-- Functie met doorgegeven max length, voorkom veel aanroepen aan maxLength
writeTableContents :: Table -> IntList -> String
writeTableContents (x:xs) maxWidths = z where z = concat([writeRow(x)(maxWidths)  ,writeTableContents(xs)(maxWidths)])
writeTableContents [] maxWidths = z where z = writeTableSeperator(maxWidths)

writeTableHeader:: IntList -> Row -> String
writeTableHeader x table = y where y = concat([writeTableSeperator(x) ,writeRow(table)(x) , writeTableSeperator(x)])

-- Functie die wordt aangeroepen
writeTable :: Table -> String
writeTable (x:xs) = z where z = concat([writeTableHeader(getMaxColumnLengths(x:xs))(x) , writeTableContents(xs)(getMaxColumnLengths(x:xs))])

-- Functie die wordt aangeroepen
project :: [String] -> Table -> Table
project columns table = createTableFromIndexList (projectGetIndexList columns table) table

createTableFromIndexList :: IntList -> Table -> Table
createTableFromIndexList indexlist (x:xs) = concat([[createColumnFromIndexList indexlist x], createTableFromIndexList indexlist xs])
createTableFromIndexList indexlist [] = []

createColumnFromIndexList :: IntList -> Row -> Row
createColumnFromIndexList (x:xs) row = concat([[getColumnFromIndex x row],createColumnFromIndexList xs row])
createColumnFromIndexList [] row = []

getColumnFromIndex :: Int -> Row -> String
getColumnFromIndex idx column
    | idx == 0 = head column
    | otherwise = getColumnFromIndex (idx-1) (tail column)

projectGetIndexList :: [String] -> Table -> IntList
projectGetIndexList (x:xs) table = concat([[getColumnIndex table x], projectGetIndexList xs table])
projectGetIndexList [] table = []

getColumnIndexInternal :: Row -> String -> Int -> Int
getColumnIndexInternal (x:xs) name begin
   | x == name = begin
   | otherwise = getColumnIndexInternal(xs)(name)(begin+1)
getColumnIndexInternal [] name begin = -1

getColumnIndex :: Table -> String -> Int
getColumnIndex (x:xs) name = z where z = getColumnIndexInternal( x )(name)( 0)


-- Functie Join
join Table -> Table -> Table
join x y = joinT (transpose x) (transpose y)

-- FUCK YOU!
