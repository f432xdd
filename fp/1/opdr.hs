import Char

rangeProduct :: Int -> Int -> Int
rangeProduct m n
    | n > m = m * rangeProduct(m+1)(n)
    | n == m = n
    | otherwise = 0