type Row = [String]
type Table = [Row]

--tableColumnLen :: Table -> [Int] -> [Int]
testrow :: Row
testrowb :: Row
testrow = ["Comp", "jf"]
testrowb = ["He", "Universiteit Utrecht"]

compilers :: Table
compilers =
  [ ["Compiler", "Universiteit/bedrijf"]
  , ["Helium", "Universiteit Utrecht"]
  , ["NHC", "University of York"]
  , ["GHC", "Microsoft Research"]
  , ["Hugs", "Galois Connections"]
  , ["Hugs.NET", "Galois Connections"]
  , ["O'Haskell", "Oregon Graduate Institute"]
  , ["O'Haskell", "Chalmers University of Technology"]
  , ["HBC", "Chalmers University of Technology"]
  ]

locaties :: Table
locaties =
  [ ["Universiteit/bedrijf", "Land", "Stad"]
  , ["Universiteit Utrecht", "Nederland", "Utrecht"]
  , ["University of York", "Engeland", "York"]
  , ["Microsoft Research", "Engeland", "Cambridge"]
  , ["Galois Connections", "Verenigde Staten", "Beaverton"]
  , ["Oregon Graduate Institute", "Verenigde Staten", "Beaverton"]
  , ["Chalmers University of Technology", "Zweden", "Goteborg"]
  ]


--tableColumnLen [] s = return s -- Return size of header
--tableColumnLen (x:xs) = tableRowLen(x) --tableColumnLen(xs)

tableRowLen :: Row -> [Int]
tableRowLen [] = []
tableRowLen (x:xs) = z where z = length(x) : tableRowLen(xs)

compareLen :: [Int] -> [Int] -> [Int]
compareLen [] [] = []
compareLen (x:xs) (y:ys) = max(x)(y) : compareLen(xs)(ys)

getMaxTableRowLen :: Table -> Row -> [Int]
getMaxTableRowLen [] [] = [];
getMaxTableRowLen (x:xs) [] = getMaxTableRowLen(xs)(x)
getMaxTableRowLen (x:xs) y = compareLen(tableRowLen(x))(tableRowLen(y))

