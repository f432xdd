module FQL where

import List

-- Defenities van deze opgave (Gegeven)

type Table = [[String]]

-- VoorGedefinieerde Tables (Gegeven)

compilers :: Table
compilers =
  [ ["Compiler", "Universiteit/bedrijf"]
  , ["Helium", "Universiteit Utrecht"]
  , ["NHC", "University of York"]
  , ["GHC", "Microsoft Research"]
  , ["Hugs", "Galois Connections"]
  , ["Hugs.NET", "Galois Connections"]
  , ["O'Haskell", "Oregon Graduate Institute"]
  , ["O'Haskell", "Chalmers University of Technology"]
  , ["HBC", "Chalmers University of Technology"]
  ]
locaties :: Table
locaties =
  [ ["Universiteit/bedrijf", "Land", "Stad"]
  , ["Universiteit Utrecht", "Nederland", "Utrecht"]
  , ["University of York", "Engeland", "York"]
  , ["Microsoft Research", "Engeland", "Cambridge"]
  , ["Galois Connections", "Verenigde Staten", "Beaverton"]
  , ["Oregon Graduate Institute", "Verenigde Staten", "Beaverton"]
  , ["Chalmers University of Technology", "Zweden", "Goteborg"]
  ]

---
--- writeTable Functies.
---

-- calculateRowLengths
-- Bereken de lengtes voor elk element van een lijst van strings.
-- @param1 - Lijst van strings waarvan de lengtes moeten worden berekend.
-- @output - Lijst van integers van de lengtes van de elementen in param1.
calculateRowLengths :: [String] -> [Int]
calculateRowLengths (x:xs) = length x : calculateRowLengths xs
calculateRowLengths [] = []

-- getMaxNumbersOfTwoLists
-- Geeft een integer lijst met op elke index de maximale waarde van de invoer lijsten op diezelfde index.
-- @param1 - Integer lijst 1
-- @param2 - Integer lijst 2
-- @output - Lijst van de maximale waardes van elementen van param1 en param2 waarvan de index hetzelfde is.
getMaxNumbersOfTwoLists :: [Int] -> [Int] -> [Int]
getMaxNumbersOfTwoLists (xa:xsa) (xb:xsb) = max xa xb : getMaxNumbersOfTwoLists xsa xsb
getMaxNumbersOfTwoLists [] (x:xs) = x : getMaxNumbersOfTwoLists [] xs
getMaxNumbersOfTwoLists (x:xs) [] = x : getMaxNumbersOfTwoLists xs []
getMaxNumbersOfTwoLists [] [] = []

-- getMaxColumnLengths 
-- De maximale lengtes per kolom van een tabel.
-- @param1 - Tabel waarvan de lengte moet worden berekend.
-- @output - Lijst van integers met de maximale lengtes van de kolommen.
getMaxColumnLengths :: Table -> [Int]
getMaxColumnLengths (x:xs) = getMaxNumbersOfTwoLists (calculateRowLengths x) (getMaxColumnLengths xs)
getMaxColumnLengths [] = []

-- writeStringNTimes
-- Schrijf de invoer string n keer naar de uitvoer string.
-- @param1 - Aantal keer dat de string geschreven moet worden.
-- @param2 - Invoer string
-- @output - Uitvoer string
writeStringNTimes :: Int-> String -> String
writeStringNTimes n s
    | n > 0 = concat [s , writeStringNTimes (n - 1) s]
    | otherwise = ""

-- writeTableSeperator
-- Schrijf een tussenlijn naar de uitvoer string
-- @param1 - Maximale kolom lengtes
-- @output - Uitvoer string
writeTableSeperator :: [Int] -> String
writeTableSeperator (x:xs) = concat ["+", writeStringNTimes x "-", writeTableSeperator xs]
writeTableSeperator [] = "+\n"

-- writeRow
-- Schrijf de inhoud van een rij van de tabel naar de uitvoer string.
-- @param1 - De rij die moet worden weergegeven.
-- @param2 - De maximale kolom lengtes van de tabel.
-- @output - Uitvoer string.
writeRow:: [String] -> [Int] -> String
writeRow (x:xs) (xa:xsa) = concat ["|", x, writeStringNTimes (xa - length x) " ", writeRow xs xsa]
writeRow [] [] = "|\n"

-- writeTableContents
-- Schrijf de waarden van de tabel naar de uitvoer string.
-- @param1 - De tabel.
-- @param2 - De maximale kolom lengtes van de tabel.
-- @output - Uitvoer string
writeTableContents :: Table -> [Int] -> String
writeTableContents (x:xs) maxWidths = concat [writeRow x maxWidths, writeTableContents xs maxWidths]
writeTableContents [] maxWidths = writeTableSeperator maxWidths

-- writeTableHeader
-- Schrijf de kop van de tabel naar de uitvoer string.
-- @param1 - De waarden van de kop van de tabel.
-- @param2 - De maximale kolom lengtes
-- @output - Uitvoer string.
writeTableHeader:: [String] -> [Int] -> String
writeTableHeader header x = concat [writeTableSeperator x, writeRow header x, writeTableSeperator x]

-- writeTable
-- Schrijf een tabel naar de uitvoer string.
-- @param1 - De tabel.
-- @output - Uitvoer string.
writeTable :: Table -> String
writeTable x = concat [writeTableHeader (head x) (getMaxColumnLengths x), writeTableContents (tail x) (getMaxColumnLengths x)]
writeTable [] = ""

--- 
--- Project functies
---

-- returnArrayIfFirstElementEqualsString
-- Geeft de kolom waarvan het eerste element gelijk is aan die van de invoer
-- @param1 - Een ge'transpose'de tabel
-- @param2 - De naam van de kolom die gevonden moet worden
-- @output - De kolom die gezocht werd
--
returnArrayIfFirstElementEqualsString :: Table -> String -> [String]
returnArrayIfFirstElementEqualsString [] x = []
returnArrayIfFirstElementEqualsString (x:xs) s
    | head(x) == s = x
    | otherwise = returnArrayIfFirstElementEqualsString xs s

-- filterColumnNames
-- Vind alle kolommen die in de invoer zijn gegeven
-- @param1 - Lijst met strings waarvan de kolommen moeten worden geretourneerd
-- @param2 - Een ge'transpose'de tabel
-- @output - De gefilterde tabel
findColumnNames :: [String] -> Table -> Table
findColumnNames [] table = []
findColumnNames (x:xs) table = returnArrayIfFirstElementEqualsString table x : findColumnNames xs table

-- project
-- Geeft de opgegeven kolommen van een tabel
-- @param1 - De namen van de kolommen die we willen hebben
-- @param2 - De invoer tabel
-- @output - De uitvoer tabel
project :: [String] -> Table -> Table
project columns table = transpose (findColumnNames columns (transpose table))

---
--- Select
---

-- getColumnIndex
-- Geeft voor een bepaalde string aan welk positie het heeft in de kolom
-- @param1 - Invoer kolom
-- @param2 - Invoer string
-- @output - Index van de string in de kolom
getColumnIndex :: [String] -> String -> Int
getColumnIndex (x:xs) s
  | x == s = 0
  | otherwise = 1 + getColumnIndex xs s

-- getColumnFromRowByIndex
-- Geeft van een rij de juiste kolom aan de hand van een index
-- @param1 - Invoer rij
-- @param2 - Index
-- @output - De string die op de positie van de index in de kolom staat
getColumnFromRowByIndex :: [String] -> Int -> String
getColumnFromRowByIndex (x:xs) idx
  | idx == 0 = x
  | otherwise = getColumnFromRowByIndex xs (idx-1)

-- ifCondition
-- Geeft alle waardes in een bepaalde rij die voldoen aan een bepaalde voorwaarde
-- @param1 - Invoer tabel
-- @param2 - Voorwaarde
-- @param3 - Kolom index
-- @output - Tabel met slechts de rijen die voldoen aan de voorwaarde
ifCondition :: Table -> (String->Bool) -> Int -> Table
ifCondition (x:xs) c i
  | c (getColumnFromRowByIndex x i) = x : ifCondition xs c i
  | otherwise = ifCondition xs c i
ifCondition [] c i = []

-- select
-- Selecteer een aantal rijen op basis van een voorwaarde
-- @param1 - Naam van de kolom die doorzocht moet worden
-- @param2 - Voorwaarde voor het selecteren van de rijen
-- @param3 - Invoer tabel
-- @output - Uitvoer tabel
select :: String -> (String -> Bool) -> Table -> Table
select s c (x:xs)  = x : ifCondition xs c (getColumnIndex x s)

---
--- Join functies
--- 

-- containsColumn
-- Bevat een opgegeven rij een string?
-- @param1 - Lijst van strings
-- @param2 - De te vinden string
-- @output - True of False.
containsColumn :: [String] -> String -> Bool
containsColumn (x:xs) s
  | x == s = True
  | otherwise = containsColumn xs s
containsColumn [] s = False

-- findCommonFieldIndexInFirstRow
-- Vind 2 gelijke kolommen in twee rijen.
-- @param1 - Rij 1
-- @param2 - Rij 2
-- @Output - (x1,x2) waar x1 de index is van de gemeenschappelijke kolom in Rij 1 en x2 de index van de gemeenschappelijke kolom van Rij 2.
findCommonFieldIndexInFirstRow :: [String] -> [String] -> (Int,Int)
findCommonFieldIndexInFirstRow (x:xs) row2
   | containsColumn row2 x = (0, getColumnIndex row2 x)
   | otherwise = z where (x1,x2) = findCommonFieldIndexInFirstRow xs row2; z = (x1+1,x2)

-- findMatch
-- Retourneert
findMatch :: Table -> String -> Int -> [String]
findMatch (x:xs) value idx
   | (getColumnFromRowByIndex x idx) == value = x
   | otherwise = findMatch xs value idx
findMatch [] value idx = []
   
-- addRowsIfNotNull
-- Retourneert een rij die is samengevoegd uit rij1 en rij 2 indien zowel rij1 en rij 2 geen lege reeksen zijn
-- @param1 - Rij 1
-- @param2 - Rij 2
-- @output - concat [a,b] als a.length > 0 en b.length > 0
addRowsIfNotNull :: [String] -> [String] -> [String]
addRowsIfNotNull [] [] = []
addRowsIfNotNull a [] = []
addRowsIfNotNull b [] = []
addRowsIfNotNull a b = concat [a, b]

-- removeColumnFromRowByIndex
-- verwijdert de opgegeven kolom uit de lijst.
-- @param1 - de lijst.
-- @param2 - Index van te verwijderen kolom.
-- @output - String lijst met de kolom van param2 verwijdert.
removeColumnFromRowByIndex :: [String] -> Int -> [String]
removeColumnFromRowByIndex (x:xs) idx
  | idx == 0 = removeColumnFromRowByIndex xs (idx-1)
  | otherwise = x : removeColumnFromRowByIndex xs (idx-1)
removeColumnFromRowByIndex [] idx = []


-- removecolumnFromTableByIndex
-- Verwijdert een opgegeven kolom van de tabel.
-- @param1 - De tabel waaruit de kolom moet worden verwijdert
-- @param2 - De Index van de Kolom die moet worden verwijdert uit de rijen.
-- @output - De tabel van param1 zonder de kolom van param2
removeColumnFromTableByIndex :: Table -> Int -> Table
removeColumnFromTableByIndex (x:xs) idx = removeColumnFromRowByIndex x idx : removeColumnFromTableByIndex xs idx
removeColumnFromTableByIndex [] idx = []


-- recJoin
-- Recursieve body van Join. retourneert een tabel van samengevoegde rijen.
-- Indien de waarde van de gemeenschappelijke kolom van tabel 1
-- voorkomt in de kolom van een van de rijen van tabel 2 worden deze 2 rijen samengevoegd en geretourneerd.
-- @param1 - Table 1
-- @param2 - Table 2
-- @param3 - (Index of common row in table1, Index of common row in table2)
-- @output - De samengevoegde tabel van Table1 en Table2 
recJoin :: Table -> Table -> (Int,Int) -> Table
recJoin (x:xs) table2 (idx1,idx2) = concat [[addRowsIfNotNull x (findMatch table2 (getColumnFromRowByIndex x idx1) idx2)], recJoin xs table2 (idx1,idx2)]
recJoin [] table2 (idx1,idx2) = []
  
  
-- Join
-- @param1 - Tabel 1
-- @param2 - Tabel 2
-- @output - retourneerd tabel 1 + tabel 2 samengevoegd tot 1 tabel  
join :: Table -> Table -> Table
join table1 table2 = z where (x1,x2) = findCommonFieldIndexInFirstRow (head table1) (head table2); z = removeColumnFromTableByIndex (recJoin table1 table2 (x1,x2)) x1